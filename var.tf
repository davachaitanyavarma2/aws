variable "aws_region" {
  default = "ap-south-1"
}

variable "ami_name" {
  default = "ami-079b5e5b3971bd10d"
}

variable "instance_type" {
  default = "t2.micro"
}
