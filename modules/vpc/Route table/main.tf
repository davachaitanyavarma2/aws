resource "aws_route_table" "public-test" {
  vpc_id = "${var.vpc_id}"

  route {
    cidr_block = "10.0.0.0/16"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "igw-040e3d3a5ce2e2b6d"
  }

  tags = {
    Name = "public-test"
  }
}

resource "aws_route_table" "private-test" {
  vpc_id = "${var.vpc_id}"

  route {
    cidr_block = "10.0.0.0/16"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "nat-0e0270cfccfaf8214"
  }
}
